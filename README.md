msi_keyboard

Dépôt AUR:
https://aur.archlinux.org/packages/msiklm-git/

Dépôt GIT:
https://github.com/Gibtnix/MSIKLM

Copier les trois fichiers dans /etc/systemd/system/

Puis:
sudo chmod 777 msiklm-afterBoot.service

sudo chmod 777 msiklm-afterSuspend.service

sudo chmod 777 msiklm-afterSleep.service

sudo systemctl daemon-reload

sudo systemctl enable msiklm-afterBoot.service

sudo systemctl enable msiklm-afterSuspend.service

sudo systemctl enable msiklm-afterSleep.service


Rétroéclairage:

wiki: https://wiki.archlinux.org/index.php/backlight#ACPI

créer le fichier:

/etc/udev/rules.d/backlight.rules

Ajouter:

ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="acpi_video0", RUN+="/bin/chgrp video /sys/class/backlight/%k/brightness"

ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="acpi_video0", RUN+="/bin/chmod g+w /sys/class/backlight/%k/brightness"

